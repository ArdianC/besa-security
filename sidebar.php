 <?php
/**
 * The template for displaying Sdibebar for Blog, Category and Single.
 *
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */
?>
<div class="three columns omega" id="main_sidebar">
<div class="sidebar_item">
    <h4>Të thërrasim ne</h4>
    <form action="#">
    <input type="text" name="emri" placeholder="emri dhe mbiemri" />
    <input type="text" name="adresa" placeholder="adresa" />
    <input type="text" name="telefoni" placeholder="telefoni" />
    <textarea name="mesazhi" id="mesazhi" cols="30" rows="7"></textarea>
    <button type="submit" class="btn btn-sent">Dërgo</button>
    </form>
</div>

<div class="sidebar_item">
    <h4>Galeria</h4>
    <figure><a href="#"><img src="<?php echo THEMEURL; ?>images/theme/news4.png" alt="" width="100%" height="150"></a></figure>
    <h5>Koncerti i Lloyd Banks</h5>
</div>
</div>
