<?php

define('THEMEDIR', 	get_template_directory() . '/');
define('THEMEURL', 	get_template_directory_uri() . '/');
define('WPURL', 	site_url('/'));
define('URL', 		home_url('/'));
define('TD', 		'besa');

get_template_part('parts/produktet-post-type');
get_template_part('parts/services-post-type');

if ( ! isset( $content_width ) )
	$content_width = 960;

if(!function_exists(client_script)){
	function client_script(){
		wp_register_style( 'skeleton_css', THEMEURL."css/skeleton.less", '', '1.0','all');
		wp_enqueue_style( 'skeleton_css' );

		wp_register_style( 'panda_less', THEMEURL."css/style.less", '', '1.0','all');
		wp_enqueue_style( 'panda_less' );
	}
}
add_action('wp_enqueue_scripts', 'client_script');
function enqueue_less_styles($tag, $handle) {

    global $wp_styles;

    $match_pattern = '/\.less$/U';

    if ( preg_match( $match_pattern, $wp_styles->registered[$handle]->src ) ) {
        $handle = $wp_styles->registered[$handle]->handle;
        $media = $wp_styles->registered[$handle]->args;
        $href = $wp_styles->registered[$handle]->src . '?ver=' . $wp_styles->registered[$handle]->ver;
        $rel = isset($wp_styles->registered[$handle]->extra['alt']) && $wp_styles->registered[$handle]->extra['alt'] ? 'alternate stylesheet' : 'stylesheet';
        $title = isset($wp_styles->registered[$handle]->extra['title']) ? "title='" . esc_attr( $wp_styles->registered[$handle]->extra['title'] ) . "'" : '';

        $tag = "<link rel='stylesheet/less' id='$handle' $title href='$href' type='text/css' media='$media' />";
    }
    return $tag;
}
add_filter( 'style_loader_tag', 'enqueue_less_styles', 5, 2);

add_action('init', 'panda_init');

function panda_init()
{
	get_template_part('inc/init');
}
if ( !function_exists( 'quote_header_scripts' ) ) {
	add_action('init', 'quote_header_scripts');
	function quote_header_scripts() {
    if(!is_admin())
    {
    	$javascripts  = wp_enqueue_script('jquery');
      $javascripts .= wp_enqueue_script('jquery-masonry');
      $javascripts .= wp_enqueue_script('jquery-ui-core');
       $javascripts .= wp_enqueue_script('jquery-ui-widget');
       $javascripts .= wp_enqueue_script('jquery-ui-slider');

      $javascripts .= wp_enqueue_script('modernizr', THEMEURL ."js/modernizr.js",array('jquery'),'1.3.3',false);

      $javascripts .= wp_enqueue_script('less', THEMEURL ."js/less.js",'','1.3.3',false);
      /*
      $javascripts .= wp_enqueue_script('tweenMax', THEMEURL ."js/TweenMax.min.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('tweenMaxscrolltoplugin', THEMEURL ."js/ScrollToPlugin.js",'','1.3.3',true);
      $javascripts .= wp_enqueue_script('tweenMaxcss', THEMEURL ."js/CSSPlugin.min.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('tweenMaxEasePack', THEMEURL ."js/EasePack.min.js",array('jquery'),'1.3.3',true);
      */

      $javascripts .= wp_enqueue_script('easing', THEMEURL ."js/jquery.easing.1.3.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('transit', THEMEURL ."js/jquery.transit.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('isotope', THEMEURL ."js/jquery.isotope.min.js",array('jquery'),'1.3.3',true);

      $javascripts .= wp_enqueue_script('responsiveslides', THEMEURL ."js/responsiveslides.min.js",array('jquery'),'1.3.3',true);


      $javascripts .= wp_enqueue_script('infinitescroll', THEMEURL ."js/jquery.infinitescroll.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('flexslider', THEMEURL ."js/jquery.flexslider-min.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('prettyPhoto', THEMEURL ."js/jquery.prettyPhoto.js",array('jquery'),'1.3.3',true);

      $javascripts .= wp_enqueue_script('ancarousel', THEMEURL ."js/jquery.ancarousel.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('media', THEMEURL ."js/media-helper.js",array('jquery'),'1.3.3',true);

      $javascripts .= wp_enqueue_script('bpopup', THEMEURL ."js/jquery.bpopup.min.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('swipebox', THEMEURL ."js/jquery.swipebox.min.js",array('jquery'),'1.3.3',true);
      $javascripts .= wp_enqueue_script('loadImage', THEMEURL ."js/jquery.loadImage.js",array('jquery'),'1.3.3',true);

      $javascripts .= wp_enqueue_script('custom', THEMEURL ."js/custom.js",array('jquery'),'1.3.3',true);
    	echo apply_filters ('child_add_javascripts',$javascripts);
    }
  }
}
add_action('init', 'quote_init');

function quote_init()
{
	get_template_part('inc/init');
}

function panda_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() )
		return $title;

	// Add the site name.
	$title .= get_bloginfo( 'name' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		$title = "$title $sep $site_description";

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 )
		$title = "$title $sep " . sprintf( __( 'Page %s', 'panda' ), max( $paged, $page ) );

	return $title;
}
add_filter( 'wp_title', 'panda_wp_title', 10, 2 );

function panda_setup() {
  //translate support
  load_theme_textdomain( 'panda', get_template_directory() . '/languages' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menu('primary', __('Main Menu', 'besa'));

  add_theme_support( 'post-thumbnails' );
  set_post_thumbnail_size( 190, 150 , 1); // default Post Thumbnail dimensions

  add_image_size( 'gallery-full', 640, 9999, true );
  add_image_size( 'gallery-thumb', 216, 9999, true );
  add_image_size( 'portfolio-full', 1200, 9999, true );
  add_image_size( 'home-thumb', 320, 257, true );

}
add_action( 'after_setup_theme', 'panda_setup' );

function location_widget_init()
{
	register_sidebar( array(
		'name' => __( 'Sidebar', 'panda' ),
		'id' => 'sidebar-widget',
		'before_widget' => '<div class="widget_module">',
		'after_widget' => '</div>',
		'before_title' => '<h2 class="module_title">',
		'after_title' => '</h2>',
	) );
}

add_action( 'widgets_init', 'location_widget_init' );

class description_walker extends Walker_Nav_Menu
{
  function start_el(&$output, $item, $depth, $args)
  {
    global $wp_query;
    $indent = ( $depth ) ? str_repeat( "\t", $depth ) : '';

    $class_names = $value = '';

    $classes = empty( $item->classes ) ? array() : (array) $item->classes;

    $class_names = join( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) );
    $class_names = ' class="'. esc_attr( $class_names ) . '"';

    $output .= $indent . '<li id="menu-item-'. $item->ID . '"' . $value . $class_names .'>';

    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';

    $prepend = '<span>';
    $append = '</span>';
    $description  = ! empty( $item->description ) ? '<span>'.esc_attr( $item->description ).'</span>' : '';

    if($depth != 0)
    {
      $description = $append = $prepend = "";
    }

    $item_output = $args->before;
    $item_output .= '<a'. $attributes .'>';
    $item_output .= $args->link_before .apply_filters( 'the_title', $item->title, $item->ID );
    $item_output .= $description.$args->link_after;
    $item_output .= '</a>';
    $item_output .= $args->after;

    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
  }
}


function produkt_pagination(){
  $paged = $_REQUEST['paged'];
  $arg = array('post_type'=> array('produkt'),'posts_per_page'=>4, 'paged'=>$paged,'orderby'=>'rand');
  $query = new wp_query($arg);

  ?>
    <div class="products_list" id="products_list">
    <?php
    if($query->have_posts()) {
      while($query->have_posts()) {
        $query->the_post();
        ?>
        <?php get_template_part('parts/produkt_template'); ?>
        <?php
      }
    }
    ?>
    </div>
  <?php

  die();
}
add_action( 'wp_ajax_nopriv_produkt_pagination', 'produkt_pagination' );
add_action( 'wp_ajax_produkt_pagination', 'produkt_pagination' );

function load_gallery(){
  $post_id = $_POST['data'];
  $post = get_post($post_id);
  echo do_shortcode($post->post_content);
  die();
}
add_action( 'wp_ajax_nopriv_load_gallery', 'load_gallery' );
add_action( 'wp_ajax_load_gallery', 'load_gallery' );

remove_shortcode('gallery');
add_shortcode('gallery', 'gallery_shortcode_main');
function gallery_shortcode_main( $attr ) {
  global $post, $wp_locale;
  $post = get_post();
  static $instance = 0;
  $instance++;
  if ( ! empty( $attr['ids'] ) ) {
    // 'ids' is explicitly ordered, unless you specify otherwise.
    if ( empty( $attr['orderby'] ) )
      $attr['orderby'] = 'post__in';
    $attr['include'] = $attr['ids'];
  }
  // Allow plugins/themes to override the default gallery template.
  $output = apply_filters('post_gallery', '', $attr);
  if ( $output != '' )
    return $output;
  // We're trusting author input, so let's at least make sure it looks like a valid orderby statement
  if ( isset( $attr['orderby'] ) ) {
    $attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
    if ( !$attr['orderby'] )
      unset( $attr['orderby'] );
  }
  extract(shortcode_atts(array(
    'order'      => 'ASC',
    'orderby'    => 'menu_order ID',
    'id'         => $post->ID,
    'itemtag'    => 'div',
    'icontag'    => 'div',
    'captiontag' => 'div',
    'columns'    => 3,
    'size'       => 'thumbnail',
    'include'    => '',
    'exclude'    => ''
  ), $attr));
  $id = intval($id);
  if ( 'RAND' == $order )
    $orderby = 'none';
  if ( !empty($include) ) {
    $_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
    $attachments = array();
    foreach ( $_attachments as $key => $val ) {
      $attachments[$val->ID] = $_attachments[$key];
    }
  } elseif ( !empty($exclude) ) {
    $attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
  } else {
    $attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
  }
  if ( empty($attachments) )
    return '';
  $itemtag = tag_escape($itemtag);
  $captiontag = tag_escape($captiontag);
  $columns = intval($columns);
  $itemwidth = $columns > 0 ? floor(100/$columns) : 100;
  $float = is_rtl() ? 'right' : 'left';
  $selector = "gallery-{$instance}";
  $gallery_style = $gallery_div = '';
  if ( apply_filters( 'use_default_gallery_style', true ) )
    $gallery_style = "";
  $size_class = sanitize_html_class( $size );
  $gallery_div = "";
  $output = apply_filters( 'gallery_style', $gallery_style . "\n\t\t" . $gallery_div );
  $output = '';
  $i = 0;
  foreach ( $attachments as $id => $attachment ) {
    //$link = wp_get_attachment_link($id, $size, false, false);
    $img_thumb = wp_get_attachment_image_src( $id, 'gallery-full');
    $output .= '<li>';
    $output .= '<img src="'.$img_thumb[0].'" alt="'. $id .'" class="attachment-full" />';
    $output .= '<a href="#" data-id="'. $id .'"></a>';
    $output .= '</li>';
  }
  return $output;
}

function panda_excerpt_length( $length ) {
  return 20;
}
add_filter( 'excerpt_length', 'panda_excerpt_length' );
//Si te duket fundi
function panda_custom_excerpt_more( $output ) {
  return $output;
}
add_filter( 'get_the_excerpt', 'panda_custom_excerpt_more' );
//More Linku
function panda_auto_excerpt_more( $more ) {
  return '';
}
add_filter( 'excerpt_more', 'panda_auto_excerpt_more' );

function linkifyYouTubeURLs($text) {
    $text = preg_replace('~
        # Match non-linked youtube URL in the wild. (Rev:20130823)
        https?://         # Required scheme. Either http or https.
        (?:[0-9A-Z-]+\.)? # Optional subdomain.
        (?:               # Group host alternatives.
          youtu\.be/      # Either youtu.be,
        | youtube\.com    # or youtube.com followed by
          \S*             # Allow anything up to VIDEO_ID,
          [^\w\-\s]       # but char before ID is non-ID char.
        )                 # End host alternatives.
        ([\w\-]{11})      # $1: VIDEO_ID is exactly 11 chars.
        (?=[^\w\-]|$)     # Assert next char is non-ID or EOS.
        (?!               # Assert URL is not pre-linked.
          [?=&+%\w.-]*    # Allow URL (query) remainder.
          (?:             # Group pre-linked alternatives.
            [\'"][^<>]*>  # Either inside a start tag,
          | </a>          # or inside <a> element text contents.
          )               # End recognized pre-linked alts.
        )                 # End negative lookahead assertion.
        [?=&+%\w.-]*        # Consume any URL (query) remainder.
        ~ix',
        '$1',
        $text);
    return $text;
}
function languages_list_footer(){
    $languages = icl_get_languages('skip_missing=0&orderby=code');
    if(!empty($languages)){
        echo '<div class="lang"><ul>';
        foreach($languages as $l){
            echo '<li>';
              echo '<a href="'.$l['url'].'">';
              $name = $l['native_name'];
              if($name == 'English'){
                  echo 'EN';
              }
              else
              {
                 echo 'AL';
              }
              echo '</a>';
            echo '</li>';
        }
        echo '</ul></div>';
    }
}
?>