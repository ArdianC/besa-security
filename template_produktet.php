
<?php
/*
 * Template Name: Produktet
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */
get_header(); ?>
<div id="top_header">
   <?php
   $header_image = get_field('header_image');
   ?>
   <img src="<?php echo $header_image; ?>" width="100%" height="150px" />
</div>

<div class="container" id="products_container">
    <div class="product_row  sixteen columns">
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/kamera.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, delectus culpa modi atque doloremque eum magni quibusdam iure corrupti magnam accusamus maiores provident. Ad minima cumque expedita ipsum natus eos.</p>
            </div>
        </div>
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/ndal.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, ut, animi dignissimos adipisci maiores harum expedita consequuntur consectetur omnis atque nesciunt provident eius mollitia enim soluta reprehenderit quidem dolores modi.</p>
            </div>
        </div>
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/termostat.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, cumque, illum, dolor soluta porro adipisci officiis nemo vel cupiditate nobis eaque optio inventore ex repellat praesentium qui at laudantium! Consequatur?</p>
            </div>
        </div>
    </div><!-- /product_row -->

    <div class="product_row sixteen columns">
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/detektor.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, delectus culpa modi atque doloremque eum magni quibusdam iure corrupti magnam accusamus maiores provident. Ad minima cumque expedita ipsum natus eos.</p>
            </div>
        </div>
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/sensor.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, ut, animi dignissimos adipisci maiores harum expedita consequuntur consectetur omnis atque nesciunt provident eius mollitia enim soluta reprehenderit quidem dolores modi.</p>
            </div>
        </div>
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/video_kamera.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, cumque, illum, dolor soluta porro adipisci officiis nemo vel cupiditate nobis eaque optio inventore ex repellat praesentium qui at laudantium! Consequatur?</p>
            </div>
        </div>
    </div><!-- product-row -->



    <div class="product_row sixteen columns">
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/panel.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum, delectus culpa modi atque doloremque eum magni quibusdam iure corrupti magnam accusamus maiores provident. Ad minima cumque expedita ipsum natus eos.</p>
            </div>
        </div>
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/drita.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, ut, animi dignissimos adipisci maiores harum expedita consequuntur consectetur omnis atque nesciunt provident eius mollitia enim soluta reprehenderit quidem dolores modi.</p>
            </div>
        </div>
        <div class="one-third column product">
            <figure>
                <img src="<?php echo get_template_directory_uri(); ?>/images/theme/produktet/termostat_kontroll.png" />
            </figure>
            <div class="product-content">
                <h4>Titulli</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolore, cumque, illum, dolor soluta porro adipisci officiis nemo vel cupiditate nobis eaque optio inventore ex repellat praesentium qui at laudantium! Consequatur?</p>
            </div>
        </div>
    </div><!-- product-row -->
</div><!--/products_container -->

<div id="services_info">
    <div class="container">
        <div class="info_text">
            <h3>Lidhu me ne per siguri ne shtepi</h3>
            <p>Siguria per shtepi eshte e domosdoshme per ty dhe familjen tuaj, dhe ne jemi zgjidhja e vetme, qe te ndiheni te sigurt per gjerat e lena ne shtepi,ateher ja ku jemi.
    Kjo gjendje e teknologjisë se komunikimit është vetëm një nga arsyet pse Besa Security është kompania numer nje e sigurimit në Kosovë</p>
        </div>

        <div class="info_buttons">
            <div class="three columns"><img src="<?php echo THEMEURL; ?>images/theme/tel.png" /><span>Telefono<strong>038 55 22 88</strong></span></div>
            <div class="three columns"><img src="<?php echo THEMEURL; ?>images/theme/mail.png" /><span>Dergo Email <strong>info@besa-s.com</strong></span></div>
            <div class="three columns"><img src="<?php echo THEMEURL; ?>images/theme/pin.png" /><span>Na vizito <strong>Lagjja Emshir</strong></span></div>
        </div>
    </div><!-- /.container -->
</div><!-- /#services_info -->
<?php get_footer(); ?>