$ = jQuery;
// Global variables that hold state
$(document).ready(function(){

	// Modified Isotope methods for gutters in masonry
	$.Isotope.prototype._getMasonryGutterColumns = function() {
		var gutter = this.options.masonry && this.options.masonry.gutterWidth || 0;
		containerWidth = this.element.width();

		this.masonry.columnWidth = this.options.masonry && this.options.masonry.columnWidth ||
		// Or use the size of the first item
		this.$filteredAtoms.outerWidth(true) ||
		// If there's no items, use size of container
		containerWidth;

		this.masonry.columnWidth += gutter;

		this.masonry.cols = Math.floor((containerWidth + gutter) / this.masonry.columnWidth);
		this.masonry.cols = Math.max(this.masonry.cols, 1);
	};

	$.Isotope.prototype._masonryReset = function() {
		// Layout-specific props
		this.masonry = {};
		// FIXME shouldn't have to call this again
		this._getMasonryGutterColumns();
		var i = this.masonry.cols;
		this.masonry.colYs = [];
		while (i--) {
			this.masonry.colYs.push(0);
		}
	};

	$.Isotope.prototype._masonryResizeChanged = function() {
		var prevSegments = this.masonry.cols;
		// Update cols/rows
		this._getMasonryGutterColumns();
		// Return if updated cols/rows is not equal to previous
		return (this.masonry.cols !== prevSegments);
	};
	_clearer = $('<div />').addClass('clearfix');
	var $sort_iso = $('#products_list');

	$('.flexslider').flexslider({
	    animation: "slide",
	    animationLoop: false,
	    itemWidth: 216,
	    itemMargin: 0,
		prevText:'',
		nextText:'',
		useCCS: false
	});

    /**
     * Besa - Home page slider
     * @type {[type]}
     */
	var home_slider = $(".home_slider");
	if(home_slider){
		if($.isFunction(home_slider.responsiveSlides))
		{
			home_slider.imagesLoaded(function(){
				home_slider.responsiveSlides({
					nav: true,
					pause: true,
					timeout: 4000,
					controls: home_slider
				});
			});
		}
	}

	$(window).smartresize(function(){

		var value = 4;
		var width = $('#iso_container').width();
		width = parseInt(width);

		var element = $('.produktet_container .products_list').find('.product');
		if(width <= 300)
		{
			value = 1;
			element.animate({'width':'100%'});
		}
	  	else if(width < 421){
			value = 2;
			element.animate({'width':'166px'});
		}else if(width >= 641)
		{
			value = 4;
			element.animate({'width':'170px'});
		}
		else if(width < 640 && width > 421)
		{
			element.animate({'width':'128px'});
			value = 3;
		}

		$sort_iso.isotope({

			masonry: { columnWidth: $sort_iso.width() / value }
		});
	});
	function init_masonory(){

		var value = 4;
		var width = $('#iso_container').width();
		width = parseInt(width);

		var element = $('.produktet_container .products_list').find('.product');
		if(width <= 300)
		{
			value = 1;
			element.animate({'width':'100%'});
		}
		else if(width > 300 && width < 421){
			value = 2;
			element.animate({'width':'166px'});
		}else if(width >= 641)
		{
			value = 4;
			element.animate({'width':'170px'});
		}
		else if(width < 640 && width > 421)
		{
			element.animate({'width':'128px'});
			value = 3;
		}else
		{
			value = 4;
		}

		$sort_iso.isotope({
			resizable: false,
			itemSelector : '.product',
			masonry: { columnWidth: $sort_iso.width() / value }
		});


	}

	// Infinite Scroll Setup
	var produktet_more_results = $(".produktet_more_results");
	var next_page = produktet_more_results.find('> a');
	var global_filter = '';

	next_page.click(function(ev){
		ev.preventDefault();
	});

	$sort_iso.infinitescroll({
        navSelector  : produktet_more_results,    // selector for the paged navigation
        nextSelector : next_page,  // selector for the NEXT link (to page 2)
        itemSelector : '.product',     // selector for all items you'll retrieve
        loading: {
            finishedMsg: 'No more pages to load.',
            img: 'http://i.imgur.com/qkKy8.gif'
          }
        },
        // call Isotope as a callback
        function( newElements ) {
			$sort_iso.isotope( 'insert', $( newElements ), { filter: global_filter });
        }
    );
	// filter items when filter link is clicked
	$('a.filter_category').click(function(){
	  var selector = $(this).attr('data-filter');
	  global_filter = selector;
	  //console.log(selector);
	  if(selector != '*')
	  {
	  	$('.show_all').css({display:'inline'});
	  }
	  else
	  {
	  	$('.show_all').css({display:'none'});
	  }
	  $sort_iso.isotope({ filter: selector });
	  return false;
	});

	init_masonory();

	var $map_element = $('.map_container');
	if($map_element.size() == 1){

		var google_lat = $map_element.data('lat');
		var google_long = $map_element.data('long');
		var icon_dest = $map_element.data('icon');
		var map;
        var prishtina = new google.maps.LatLng(google_lat,google_long);

        function initialize() {

          var featureOpts = [
            {
                "featureType": "water",
                "stylers": [
                    {
                        "color": "#021019"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "stylers": [
                    {
                        "color": "#232323"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#93786f"
                    },
                    {
                        "lightness": 5
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#91796d"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#91796d"
                    },
                    {
                        "lightness": 25
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#93786f"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#93786f"
                    },
                    {
                        "lightness": 16
                    }
                ]
            },
            {
                "featureType": "road.local",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#94796e"
                    }
                ]
            },

            {
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "elementType": "labels.text.stroke",
                "stylers": [
                    {
                        "color": "#000000"
                    },
                    {
                        "lightness": 13
                    }
                ]
            },
            {
                "featureType": "transit",
                "stylers": [
                    {
                        "color": "#146474"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#000000"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "color": "#144b53"
                    },
                    {
                        "lightness": 14
                    },
                    {
                        "weight": 1.4
                    }
                ]
            }
        ];

        var myOptions = {
            zoom: 15,
            center: prishtina,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            scrollwheel: false,
            styles: featureOpts,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById('map_canvas'), myOptions);
        setSingleMarker(map);
        }

        function setSingleMarker(map) {

            var marker = new google.maps.Marker({
                map:map,
                position: new google.maps.LatLng(google_lat,google_long),
                icon: icon_dest + '/images/panda_pin.png'
          });
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    }


    //popup content
    $('#popup_btn').live('click', function(e) {
        e.preventDefault();
        $('#portContainer').bPopup();
    });

    $('#portContainer .flexslider_home').flexslider({
        animation: "slide",
        controlNav: true,
        prevText: "",
        nextText: "",
    });
});
