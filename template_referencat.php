<?php
/*
 * Template Name: Referencat
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */

get_header(); ?>

<script type="text/javascript">
function initialize() {
    var styles = [
        {
          stylers: [
            { hue: "#0c4da2" },
            { saturation: -20 }
          ]
        },{
          featureType: "road",
          elementType: "geometry",
          stylers: [
            { lightness: 100 },
            { visibility: "simplified" }
          ]
        },{
          featureType: "road",
          elementType: "labels",
          stylers: [
            { visibility: "on" }
          ]
        }
    ];

    var mapOptions = {
      center: new google.maps.LatLng(42.672421, 21.164539),
      zoom: 11,
      mapTypeControl: false,
      scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

     var map = new google.maps.Map(document.getElementById('map'), mapOptions);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<div class="container" id="map_container">
<h3>Prezenca jonë në territorin e kosovës:</h3>
    <div id="map">

    </div><!-- /map -->
</div><!-- /.container -->


<div class="container" id="partners_container">
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/ipko.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/ministria.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/beer.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/rtk.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/vala.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/ubt.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/teb.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/zmobile.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
</div><!-- /.container -->
<?php get_footer(); ?>