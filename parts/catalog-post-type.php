<?php
function catalogposttype() {
    /**
     * Enable the Catalog custom post type
     * http://codex.wordpress.org/Function_Reference/register_post_type
     */
    $labels = array(
        'name' => __( 'Catalog', 'catalogposttype','panda' ),
        'singular_name' => __( 'Catalog Item', 'catalogposttype','panda' ),
        'add_new' => __( 'Add New Item', 'catalogposttype','panda' ),
        'add_new_item' => __( 'Add New Catalog Item', 'catalogposttype' ,'panda'),
        'edit_item' => __( 'Edit Catalog Item', 'catalogposttype','panda' ),
        'new_item' => __( 'Add New Catalog Item', 'catalogposttype','panda' ),
        'view_item' => __( 'View Item', 'catalogposttype' ,'panda'),
        'search_items' => __( 'Search Catalog', 'catalogposttype','panda' ),
        'not_found' => __( 'No catalog items found', 'catalogposttype','panda' ),
        'not_found_in_trash' => __( 'No catalog items found in trash', 'catalogposttype','panda' )
    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'supports' => array( 'title','thumbnail'),
        'capability_type' => 'post',
        'rewrite' => array("slug" => "catalog"), // Permalinks format
        'menu_position' => 5,
        'has_archive' => true
    ); 
    register_post_type( 'catalog', $args );

    
    /**
     * Register a taxonomy for Catalog Categories
     * http://codex.wordpress.org/Function_Reference/register_taxonomy
     */
    $taxonomy_catalog_category_labels = array(
        'name' => _x( 'Catalog Categories', 'catalogposttype' ,'panda'),
        'singular_name' => _x( 'Catalog Category', 'catalogposttype','panda' ),
        'search_items' => _x( 'Search Catalog Categories', 'catalogposttype' ,'panda'),
        'popular_items' => _x( 'Popular Catalog Categories', 'catalogposttype' ,'panda'),
        'all_items' => _x( 'All Catalog Categories', 'catalogposttype','panda' ),
        'parent_item' => _x( 'Parent Catalog Category', 'catalogposttype' ,'panda'),
        'parent_item_colon' => _x( 'Parent Catalog Category:', 'catalogposttype' ,'panda'),
        'edit_item' => _x( 'Edit Catalog Category', 'catalogposttype' ,'panda'),
        'update_item' => _x( 'Update Catalog Category', 'catalogposttype' ,'panda'),
        'add_new_item' => _x( 'Add New Catalog Category', 'catalogposttype' ,'panda'),
        'new_item_name' => _x( 'New Catalog Category Name', 'catalogposttype','panda' ),
        'separate_items_with_commas' => _x( 'Separate catalog categories with commas', 'catalogposttype' ,'panda'),
        'add_or_remove_items' => _x( 'Add or remove catalog categories', 'catalogposttype' ,'panda'),
        'choose_from_most_used' => _x( 'Choose from the most used catalog categories', 'catalogposttype' ,'panda'),
        'menu_name' => _x( 'Catalog Categories', 'catalogposttype','panda' ),
    );
    
    $taxonomy_catalog_category_args = array(
        'labels' => $taxonomy_catalog_category_labels,
        'public' => true,
        'show_in_nav_menus' => true,
        'show_ui' => true,
        'show_tagcloud' => true,
        'hierarchical' => true,
        'rewrite' => true,
        'query_var' => true
    );
    
    register_taxonomy( 'catalog_category', array( 'catalog' ), $taxonomy_catalog_category_args );
    
    
}
add_action( 'init', 'catalogposttype' );
// Allow thumbnails to be used on catalog post type
add_theme_support( 'post-thumbnails', array( 'catalog' ) );
 
/**
 * Add Columns to Catalog Edit Screen
 * http://wptheming.com/2010/07/column-edit-pages/
 */
 
function catalogposttype_edit_columns($catalog_columns){
    $catalog_columns = array(
        "cb" => "<input type=\"checkbox\" />",
        "title" => _x('Title', 'column name','panda'),
        "thumbnail" => __('Thumbnail', 'catalogposttype'),
        "catalog_category" => __('Category', 'catalogposttype'),
        "catalog_tag" => __('Tags', 'catalogposttype'),
        "author" => __('Author', 'catalogposttype'),
        "comments" => __('Comments', 'catalogposttype'),
        "date" => __('Date', 'catalogposttype'),
    );
    $catalog_columns['comments'] = '<div class="vers"><img alt="Comments" src="' . esc_url( admin_url( 'images/comment-grey-bubble.png' ) ) . '" /></div>';
    return $catalog_columns;
}
add_filter( 'manage_edit-catalog_columns', 'catalogposttype_edit_columns' );
 
/**
 * Add Catalog count to "Right Now" Dashboard Widget
 */
function add_catalog_counts() {
        if ( ! post_type_exists( 'catalog' ) ) {
             return;
        }
        $num_posts = wp_count_posts( 'catalog' );
        $num = number_format_i18n( $num_posts->publish );
        $text = _n( 'Catalog Item', 'Catalog Items', intval($num_posts->publish) );
        if ( current_user_can( 'edit_posts' ) ) {
            $num = "<a href='edit.php?post_type=catalog'>$num</a>";
            $text = "<a href='edit.php?post_type=catalog'>$text</a>";
        }
        echo '<td class="first b b-catalog">' . $num . '</td>';
        echo '<td class="t catalog">' . $text . '</td>';
        echo '</tr>';
        if ($num_posts->pending > 0) {
            $num = number_format_i18n( $num_posts->pending );
            $text = _n( 'Catalog Item Pending', 'Catalog Items Pending', intval($num_posts->pending) );
            if ( current_user_can( 'edit_posts' ) ) {
                $num = "<a href='edit.php?post_status=pending&post_type=catalog'>$num</a>";
                $text = "<a href='edit.php?post_status=pending&post_type=catalog'>$text</a>";
            }
            echo '<td class="first b b-catalog">' . $num . '</td>';
            echo '<td class="t catalog">' . $text . '</td>';
            echo '</tr>';
        }
}
add_action( 'right_now_content_table_end', 'add_catalog_counts' );
/**
 * Add contextual help menu
 */
 
function catalogposttype_add_help_text( $contextual_help, $screen_id, $screen ) { 
    if ( 'catalog' == $screen->id ) {
        $contextual_help =
        '<p>' . __('The title field and the big Post Editing Area are fixed in place, but you can reposition all the other boxes using drag and drop, and can minimize or expand them by clicking the title bar of each box. Use the Screen Options tab to unhide more boxes (Excerpt, Send Trackbacks, Custom Fields, Discussion, Slug, Author) or to choose a 1- or 2-column layout for this screen.','panda') . '</p>' .
        '<p>' . __('<strong>Title</strong> - Enter a title for your post. After you enter a title, you&#8217;ll see the permalink below, which you can edit.','panda') . '</p>' .
        '<p>' . __('<strong>Post editor</strong> - Enter the text for your post. There are two modes of editing: Visual and HTML. Choose the mode by clicking on the appropriate tab. Visual mode gives you a WYSIWYG editor. Click the last icon in the row to get a second row of controls. The HTML mode allows you to enter raw HTML along with your post text. You can insert media files by clicking the icons above the post editor and following the directions. You can go the distraction-free writing screen, new in 3.2, via the Fullscreen icon in Visual mode (second to last in the top row) or the Fullscreen button in HTML mode (last in the row). Once there, you can make buttons visible by hovering over the top area. Exit Fullscreen back to the regular post editor.','panda') . '</p>' .
        '<p>' . __('<strong>Publish</strong> - You can set the terms of publishing your post in the Publish box. For Status, Visibility, and Publish (immediately), click on the Edit link to reveal more options. Visibility includes options for password-protecting a post or making it stay at the top of your blog indefinitely (sticky). Publish (immediately) allows you to set a future or past date and time, so you can schedule a post to be published in the future or backdate a post.','panda') . '</p>' .
        ( ( current_theme_supports( 'post-formats' ) && post_type_supports( 'post', 'post-formats' ) ) ? '<p>' . __( '<strong>Post Format</strong> - This designates how your theme will display a specific post. For example, you could have a <em>standard</em> blog post with a title and paragraphs, or a short <em>aside</em> that omits the title and contains a short text blurb. Please refer to the Codex for <a href="http://codex.wordpress.org/Post_Formats#Supported_Formats">descriptions of each post format</a>. Your theme could enable all or some of 10 possible formats.','panda' ) . '</p>' : '' ) .
        '<p>' . __('<strong>Featured Image</strong> - This allows you to associate an image with your post without inserting it. This is usually useful only if your theme makes use of the featured image as a post thumbnail on the home page, a custom header, etc.','panda') . '</p>' .
        '<p>' . __('<strong>Send Trackbacks</strong> - Trackbacks are a way to notify legacy blog systems that you&#8217;ve linked to them. Enter the URL(s) you want to send trackbacks. If you link to other WordPress sites they&#8217;ll be notified automatically using pingbacks, and this field is unnecessary.','panda') . '</p>' .
        '<p>' . __('<strong>Discussion</strong> - You can turn comments and pings on or off, and if there are comments on the post, you can see them here and moderate them.','panda') . '</p>' .
        '<p><strong>' . __('For more information:','panda') . '</strong></p>' .
        '<p>' . __('<a href="http://codex.wordpress.org/Posts_Add_New_Screen" target="_blank">Documentation on Writing and Editing Posts</a>','panda') . '</p>' .
        '<p>' . __('<a href="http://wordpress.org/support/" target="_blank">Support Forums</a>','panda') . '</p>';
  } elseif ( 'edit-catalog' == $screen->id ) {
    $contextual_help = 
        '<p>' . __('You can customize the display of this screen in a number of ways:','panda') . '</p>' .
        '<ul>' .
        '<li>' . __('You can hide/display columns based on your needs and decide how many posts to list per screen using the Screen Options tab.','panda') . '</li>' .
        '<li>' . __('You can filter the list of posts by post status using the text links in the upper left to show All, Published, Draft, or Trashed posts. The default view is to show all posts.','panda') . '</li>' .
        '<li>' . __('You can view posts in a simple title list or with an excerpt. Choose the view you prefer by clicking on the icons at the top of the list on the right.','panda') . '</li>' .
        '<li>' . __('You can refine the list to show only posts in a specific category or from a specific month by using the dropdown menus above the posts list. Click the Filter button after making your selection. You also can refine the list by clicking on the post author, category or tag in the posts list.','panda') . '</li>' .
        '</ul>' .
        '<p>' . __('Hovering over a row in the posts list will display action links that allow you to manage your post. You can perform the following actions:','panda') . '</p>' .
        '<ul>' .
        '<li>' . __('Edit takes you to the editing screen for that post. You can also reach that screen by clicking on the post title.','panda') . '</li>' .
        '<li>' . __('Quick Edit provides inline access to the metadata of your post, allowing you to update post details without leaving this screen.','panda') . '</li>' .
        '<li>' . __('Trash removes your post from this list and places it in the trash, from which you can permanently delete it.','panda') . '</li>' .
        '<li>' . __('Preview will show you what your draft post will look like if you publish it. View will take you to your live site to view the post. Which link is available depends on your post&#8217;s status.','panda') . '</li>' .
        '</ul>' .
        '<p>' . __('You can also edit multiple posts at once. Select the posts you want to edit using the checkboxes, select Edit from the Bulk Actions menu and click Apply. You will be able to change the metadata (categories, author, etc.) for all selected posts at once. To remove a post from the grouping, just click the x next to its name in the Bulk Edit area that appears.','panda') . '</p>' .
        '<p><strong>' . __('For more information:','panda') . '</strong></p>' .
        '<p>' . __('<a href="http://codex.wordpress.org/Posts_Screen" target="_blank">Documentation on Managing Posts</a>','panda') . '</p>' .
        '<p>' . __('<a href="http://wordpress.org/support/" target="_blank">Support Forums</a>','panda') . '</p>';
  }
  return $contextual_help;
}
add_action( 'contextual_help', 'catalogposttype_add_help_text', 10, 3 );
/**
 * Displays the custom post type icon in the dashboard
 */
function catalogposttype_catalog_icons() { ?>
    <style type="text/css" media="screen">
        #menu-posts-catalog .wp-menu-image {
            background: url(<?php echo get_template_directory_uri(); ?>/images/catalog-icon.png) no-repeat 6px 6px !important;
        }
        #menu-posts-catalog:hover .wp-menu-image, #menu-posts-catalog.wp-has-current-submenu .wp-menu-image {
            background-position:6px -16px !important;
        }
        #icon-edit.icon32-posts-catalog {background: url(<?php echo get_template_directory_uri(); ?>/images/catalog-32x32.png) no-repeat;}
    </style>
<?php }
add_action( 'admin_head', 'catalogposttype_catalog_icons' );
?>