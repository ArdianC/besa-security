<div class="content container">
	<div class="sixteen columns">
		<h2 class="title"><?php _e('Our Clients', TD); ?></h2>
		<!-- clients -->
		<ul class="partners_carousel clearfix" data-autoswitch="<?php echo $crystal_clients_carousel_timeout; ?>">
			<?php
			foreach($clients as $i => $entry):
				$url = $entry['url'];
				$logo_img = WPURL . $entry['logo_img'];
				?>
				<li>
					<a href="<?php echo $url ? $url : '#'; ?>">
						<img src="<?php echo $logo_img; ?>" alt="client_0<?php echo $i + 1; ?>" />
					</a>
				</li>
				<?php
			endforeach;
			?>
		</ul>
		<!-- end: clients -->
	</div>
</div>
