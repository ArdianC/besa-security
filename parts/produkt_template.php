<?php
/* Produkt Template */
$taxonomy =  'produkt_category';
$produkt_category =get_the_terms( get_the_ID(), $taxonomy );
$slug;
foreach($produkt_category as $category){
	$slug = $category->slug;
}
?>
<div class="product <?php echo $slug; ?>" id="product">
	<?php the_post_thumbnail('full'); ?>
	<span class="title"><?php the_title(); ?></span>
	<div class="product_hover">
		<span class="title"><?php the_title(); ?></span>
		<?php the_content(); ?>
	</div>
</div>