<?php
/*
 * Template Name: Fillimi
 *
 * @package WordPress
 * @subpackage Besa
 * @since Besa 1.0
 */
 ?>
<?php get_header(); ?>

<?php
  $field = get_field('select_slider');
  if($field) {
    ?>
    <div id="main_slider">
      <ul class="home_slider">
    <?php
    if(get_field('home_slider')):
      while(has_sub_field('home_slider')):
        $image = wp_get_attachment_image(get_sub_field('slider_image'), 'full');
        $caption = get_sub_field('slider_caption');
        $url = get_sub_field('slider_url');
        $post = get_sub_field('slider_post');
        $post_url = $post->guid;
        ?>
        <li><a href="<?php echo (isset($url)) ? $url : $post_url; ?>"><?php echo $image; ?></a></li>
        <?php
        endwhile;
      endif;
      ?>
      </ul>
    </div>
    <?php
  }
  else {
    ?>
    <div id="main_slider">
      <ul class="home_slider">
      <?php
      $args = array(
      'post_type' => 'produkt',
      'post_status' => 'publish',
      'posts_per_page' => 5
      );
      $slider_content = new WP_Query($args);
      if($slider_content->have_posts()):
        while($slider_content->have_posts()): $slider_content->the_post();
          $image_url =wp_get_attachment_image_src(get_post_thumbnail_id(), 'full')[0];
          ?>
          <li><a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url; ?>" alt="<?php the_title(); ?>"></a></li>
          <?php
        endwhile;
      endif;
      ?>
      </ul>
    </div><!-- /#main_slider -->
    <?php
  }
?>

<div class="container">
  <div class="sixteen columns" id="main_content">
      <div class="four columns alpha item">
        <figure>
          <a href="#">
            <img src="<?php echo THEMEURL; ?>images/theme/home1.jpg" alt="">
          </a>
            <span class="image_icon"><img src="<?php echo THEMEURL; ?>images/theme/round_home.png" alt="" /></span>
        </figure>
        <h3><?php echo __('Shtëpi', 'besa'); ?></h3>
        <ul class="list_details">
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/alarm.png" alt=""/></span><span>Alarm i thyrjes</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/alarm2.png" alt=""/></span><span>Alarm kunder zjarrit</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/kamera.png" alt=""/></span><span>Kamera</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/roje.png" alt=""/></span><span>Roje Fizike</span></li>
        </ul>
      </div>

      <div class="four columns alpha item">
        <figure>
          <a href="#">
            <img src="<?php echo THEMEURL; ?>images/theme/home2.jpg" alt="">
          </a>
            <span class="image_icon"><img src="<?php echo THEMEURL; ?>images/theme/round_build.png" alt="" /></span>
        </figure>
        <h3><?php echo __('Biznese të vogla', 'besa'); ?></h3>
        <ul class="list_details">
         <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/alarm.png" alt=""/></span><span>Alarm i thyrjes</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/alarm2.png" alt=""/></span><span>Alarm kunder zjarrit</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/kamera.png" alt=""/></span><span>Kamera</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/plus.png" alt=""/></span><span>Sherbime Plus</span></li>
        </ul>
      </div>

      <div class="four columns alpha item">
        <figure>
          <a href="#">
            <img src="<?php echo THEMEURL; ?>images/theme/home3.jpg" alt="">
          </a>
            <span class="image_icon"><img src="<?php echo THEMEURL; ?>images/theme/round_uni.png" alt="" /></span>
        </figure>
        <h3><?php echo __('Korporata/Institucione', 'besa'); ?></h3>
        <ul class="list_details">
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/panik.png" alt=""/></span><span>Panik Buton</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/transport.png" alt=""/></span><span>Transporti i Parave</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/mbrojtje.png" alt=""/></span><span>Mbrojtje Fizike VIP</span></li>
          <li><span class="icon"><img src="<?php echo THEMEURL; ?>images/theme/tubime.png" alt=""/></span><span>Tubime Publike</span></li>
        </ul>
      </div>

      <div class="four columns alpha item" id="main_sidebar">
        <div class="sidebar_item">
            <h4>Të thërrasim ne</h4>
            <form action="#" method="post">
            <input type="text" name="emri" placeholder="emri dhe mbiemri" />
            <input type="text" name="adresa" placeholder="adresa" />
            <input type="text" name="telefoni" placeholder="telefoni" />
            <textarea name="mesazhi" id="mesazhi" cols="30" rows="7"></textarea>
            <button type="submit" class="btn btn-sent">Dërgo</button>
            </form>
        </div>
      </div><!-- /#main_sidebar -->
    </div><!-- /.sixteen columns -->

    <div class="sixteen columns" id="main_news">
      <div class="news_content">
        <?php
          $args = array(
            'posts_per_page' => 5
          );
          query_posts($args);
        ?>
        <h4><?php echo __('Të rejat', 'besa'); ?></h4>
        <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
        <?php $image_url =wp_get_attachment_image_src(get_post_thumbnail_id(), 'thumbnail')[0]; ?>
        <div class="four columns news_item">
          <figure><a href="<?php the_permalink(); ?>"><img src="<?php echo $image_url; ?>" alt="<?php the_title(); ?>" width="100%" height="180"></a></figure>
            <h5><?php the_title(); ?></h5>
            <p><?php the_excerpt(); ?></p>
        </div>
        <?php endwhile; endif; wp_reset_postdata(); ?>

      <div class="gallery_content">
        <div class="four columns news_item">
          <div class="sidebar_item">
            <!-- <h4>Galeria</h4> -->
            <figure><a href="#"><img src="<?php echo THEMEURL; ?>images/theme/news4.png" alt="" width="100%" height="180"></a></figure>
            <h5>Koncerti i Lloyd Banks</h5>
        </div>
        </div>
      </div><!-- /gallery_content -->
      </div><!-- /.sixteen_columns -->
  </div><!-- /#main_content -->
</div><!-- /.container -->

<?php get_footer(); ?>