<?php
/*
 * Template Name: Partneret
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */

get_header(); ?>
<div id="top_header">
   <?php
   $header_image = get_field('header_image');
   ?>
   <img src="<?php echo $header_image; ?>" width="100%" height="150px" />
</div>

<div class="container" id="partners_container">
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/ipko.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/ministria.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/beer.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/rtk.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/vala.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/ubt.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/teb.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>

    <div class="four columns partner">
        <figure>
            <img src="<?php echo get_template_directory_uri(); ?>/images/theme/partneret/zmobile.png" alt="" />
        </figure>
        <h4>Partner 1</h4>
    </div>
</div><!-- /.container -->

<?php get_footer(); ?>