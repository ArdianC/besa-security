<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
* @package WordPress
 * @subpackage Besa
 * @since Besa 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	 <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="images/favicon.ico">
	<link rel="apple-touch-icon" href="images/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="images/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="images/apple-touch-icon-114x114.png">
	<script type="text/javascript"  src="https://maps.googleapis.com/maps/api/js?sensor=true"></script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div class="container">
	<div class="sixteen columns" id="main_header">
		<div class="five columns alpha" id="left_logo">
			<a href="<?php echo get_option('home'); ?>"><img src="<?php echo THEMEURL; ?>/images/theme/logo.png" alt="" /></a>
		</div>

		<div class="five columns omega" id="right_logo">
			<div class="tel">
				<img src="<?php echo THEMEURL; ?>/images/theme/tel.png" />
				<span>038/044/049 <br/><strong>54 55 54</strong></span>
			</div>
			<a href="<?php echo get_option('home'); ?>"><img src="<?php echo THEMEURL; ?>/images/theme/logo_right.png" alt="" /></a>
		</div><!-- /#right_logo -->
	</div><!-- ./#header_container -->

	<div class="sixteen columns clearfix" id="main_menu">
		<nav>
			<ul>
				<?php
					wp_nav_menu(array(
						'theme_location' =>"primary",
						'container' => ''
					));
				?>
			</ul>
		</nav>
	</div>
</div><!--/.container -->

