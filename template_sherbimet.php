
<?php
/*
 * Template Name: Sherbimet
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */
get_header(); ?>
<div id="top_header">
   <?php
   $header_image = get_field('header_image');
   ?>
   <img src="<?php echo $header_image; ?>" width="100%" height="150px" />
</div>

<div class="container">
    <div class="sixteen columns">
        <div  id="services_icons">
            <div class="two columns" id="first_service">
                <a href="#"><img src="<?php echo THEMEURL; ?>images/theme/round_home.png" alt="" /></a>
            </div><!--/#first_service -->

            <div class="two columns" id="second_service">
                 <a href="#"><img src="<?php echo THEMEURL; ?>images/theme/round_build.png" alt="" /></a>
            </div><!-- /#second_service -->

            <div class="two columns" id="third_service">
                <a href="#"><img src="<?php echo THEMEURL; ?>images/theme/round_uni.png" alt="" /></a>
            </div><!-- /#third_service -->
        </div>
    </div><!-- /#services-icons -->


    <div class="sixteen columns" id="services_posts">
        <div class="eight columns post_item">
            <figure>
                <img src="<?php echo THEMEURL; ?>images/theme/services_post1.png" alt="" width="100%" height="200px" />
                <span class="post_icon"><img src="<?php echo THEMEURL; ?>images/theme/s1.png" /></span>
            </figure>
            <h3>Alarm i thyerjes</h3>
            <p>Qendrat Monitoruese të BESA SECURITY ndihmojnë në mbrojtjen e njerëzve dhe
            gjerave që ju i çmoni më së shumti, 24 orë non-stop. Ne e formojmë një rrjet gjithë
            përfshirës të qendrave komanduese të ndërlidhura dhe të vendosura në mënyrë
            strategjike nëpër Kosovë, duke i mundësuar Besa Security që në mënyrë efikase të
            ofroj sigurim të vazhdueshëm në vendin esiguruar me Alarm. Kjo gjendje e teknologjisë
            se komunikimit është vetëm një nga arsyet pse Besa Security është kompania Nr.1 e
            sigurimit në Kosovë.</p>
        </div>

        <div class="eight columns post_item">
            <figure>
                <img src="<?php echo THEMEURL; ?>images/theme/services_post2.png" alt="" width="100%" height="200px" />
                <span class="post_icon"><img src="<?php echo THEMEURL; ?>images/theme/s2.png" /></span>
            </figure>
            <h3>Alarm kunder zjarrit</h3>
            <p>Qendrat Monitoruese të BESA SECURITY ndihmojnë në mbrojtjen e njerëzve dhe
            gjerave që ju i çmoni më së shumti, 24 orë non-stop. Ne e formojmë një rrjet gjithë
            përfshirës të qendrave komanduese të ndërlidhura dhe të vendosura në mënyrë
            strategjike nëpër Kosovë, duke i mundësuar Besa Security që në mënyrë efikase të
            ofroj sigurim të vazhdueshëm në vendin esiguruar me Alarm. Kjo gjendje e teknologjisë
            se komunikimit është vetëm një nga arsyet pse Besa Security është kompania Nr.1 e
            sigurimit në Kosovë.</p>
        </div>

        <div class="eight columns post_item">
            <figure>
                <img src="<?php echo THEMEURL; ?>images/theme/services_post3.png" alt="" width="100%" height="200px" />
                <span class="post_icon"><img src="<?php echo THEMEURL; ?>images/theme/s3.png" /></span>
            </figure>
            <h3>Alarm i thyerjes</h3>
            <p>Qendrat Monitoruese të BESA SECURITY ndihmojnë në mbrojtjen e njerëzve dhe
            gjerave që ju i çmoni më së shumti, 24 orë non-stop. Ne e formojmë një rrjet gjithë
            përfshirës të qendrave komanduese të ndërlidhura dhe të vendosura në mënyrë
            strategjike nëpër Kosovë, duke i mundësuar Besa Security që në mënyrë efikase të
            ofroj sigurim të vazhdueshëm në vendin esiguruar me Alarm. Kjo gjendje e teknologjisë
            se komunikimit është vetëm një nga arsyet pse Besa Security është kompania Nr.1 e
            sigurimit në Kosovë.</p>
        </div>

        <div class="eight columns post_item">
            <figure>
                <img src="<?php echo THEMEURL; ?>images/theme/services_post4.png" alt="" width="100%" height="200px" />
                <span class="post_icon"><img src="<?php echo THEMEURL; ?>images/theme/s4.png" /></span>
            </figure>
            <h3>Alarm kunder zjarrit</h3>
            <p>Qendrat Monitoruese të BESA SECURITY ndihmojnë në mbrojtjen e njerëzve dhe
            gjerave që ju i çmoni më së shumti, 24 orë non-stop. Ne e formojmë një rrjet gjithë
            përfshirës të qendrave komanduese të ndërlidhura dhe të vendosura në mënyrë
            strategjike nëpër Kosovë, duke i mundësuar Besa Security që në mënyrë efikase të
            ofroj sigurim të vazhdueshëm në vendin esiguruar me Alarm. Kjo gjendje e teknologjisë
            se komunikimit është vetëm një nga arsyet pse Besa Security është kompania Nr.1 e
            sigurimit në Kosovë.</p>
        </div>
    </div>
</div><!-- /.container -->

<div id="services_info">
    <div class="container">
        <div class="info_text">
            <h3>Lidhu me ne per siguri ne shtepi</h3>
            <p>Siguria per shtepi eshte e domosdoshme per ty dhe familjen tuaj, dhe ne jemi zgjidhja e vetme, qe te ndiheni te sigurt per gjerat e lena ne shtepi,ateher ja ku jemi.
    Kjo gjendje e teknologjisë se komunikimit është vetëm një nga arsyet pse Besa Security është kompania numer nje e sigurimit në Kosovë</p>
        </div>

        <div class="info_buttons">
            <div class="three columns"><img src="<?php echo THEMEURL; ?>images/theme/tel.png" /><span>Telefono<strong>038 55 22 88</strong></span></div>
            <div class="three columns"><img src="<?php echo THEMEURL; ?>images/theme/mail.png" /><span>Dergo Email <strong>info@besa-s.com</strong></span></div>
            <div class="three columns"><img src="<?php echo THEMEURL; ?>images/theme/pin.png" /><span>Na vizito <strong>Lagjja Emshir</strong></span></div>
        </div>
    </div><!-- /.container -->
</div><!-- /#services_info -->
<?php get_footer(); ?>