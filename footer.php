<?php
/**
 * @package WordPress
 * @subpackage Besa
 * @since Besa 1.0
 */
?>
<div id="main_footer">
    <div class="container">
        <div class="sixteen columns">
        <?php
        if(get_field('footer_info', 'options')):
            while(has_sub_field('footer_info', 'options')):
                $qyteti = get_sub_field('footer_qyteti');
                $adresa = get_sub_field('footer_adresa');
                $tel = get_sub_field('footer_tel');
                ?>
                <div class="four columns alpha">
                <h4><?php echo $qyteti; ?></h4>
                <?php echo $adresa; ?>
                <p><img src="<?php echo THEMEURL; ?>images/theme/small_tel.png"> <?php echo $tel; ?></p>
                </div>
                <?php
            endwhile;
        endif;
        ?>
        <div class="four columns omega" id="social_icons">
            <h4>Tirana</h4>
            <p>Rruga “Xhorxh W Bush”, Pallati 40-42</p>
            <p>Kulla 1, Ap5</p>
            <p><img src="<?php echo THEMEURL; ?>images/theme/small_tel.png"> 069 6570026</p>
        </div>
        </div><!-- /.sixteen columns -->
    </div><!-- /.container -->
</div><!-- /#main_footer -->
<?php wp_footer(); ?>
</body>
</html>