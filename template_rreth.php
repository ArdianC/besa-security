<?php
/*
 * Template Name: Per ne
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */

get_header(); ?>
<div id="top_header">
   <?php
   $header_image = get_field('header_image');
   ?>
   <img src="<?php echo $header_image; ?>" width="100%" height="150px" />
</div>

<div class="container">
    <div class="sixteen columns">
        <div class="about_header">
            <img src="<?php echo THEMEURL; ?>images/theme/about1.jpg" />
        </div>
        <div class="five columns">
            <h3>Pse Besa Security</h3>
            <p>Besa Security është themeluar ne vitin 2003 nga
            biznesmeni shumë i suksesshëm Abedin Hasani dhe
            është dege e BESA GROUP (BESA-COLA, Besa Ferma,
            Besa Auto, Bazzar, Marimanga).</p>
        </div>

        <div class="five columns middle">
            <h3>Monitorimi i alarmit te thyerjes</h3>
            <p>Besa Security synon që të jetë kompania me e avancuar
            në rajon në fushën e sigurisë, të sjellë në Kosovë sistemet
            e teknologjisë së fundit dhe të mbuloj tërë Kosovën.</p>
        </div>

        <div class="five columns">
            <h3>Monitorimi i alarmit kunder zjarrit</h3>
            <p>Besa Security është themeluar me qëllim të ofrimit të
            shërbimeve kulminante në fushën e sigurimit themelor,
            mbrojtjes se afërt,transportit te Parave te Gatshme
            (TPG).</p>
        </div>
    </div>


    <div class="sixteen columns about_posts">
        <div class="about_header">
            <img src="<?php echo THEMEURL; ?>images/theme/about2.jpg" />
        </div>
        <div class="five columns">
            <h3>Monitorimi i kamerave</h3>
            <p>Besa Security me teknologji bashkekohore te monitorimit
            permanent me kamera dhe qasje proaktive ne sigurimin e
            objekteve ben detektimin e hershem te kercenimit,
            thyerjes dhe menjehere dergon patrullat intervenuese per
            parandalim dhe njofton Policine.</p>
        </div>

        <div class="five columns middle">
            <h3>Sigurimi i objekteve me roje fizike</h3>
            <p>Sigurimi efikas i objekteve me punetore te sigurimit,
            brenda perimetrit te objektit, te cilet jane te uniformuar,
            te trajnuar dhe te licencuar, me mbeshtetje permanente
            te Qendres se Kontrollit dhe Patrullave Intervenuese te
            Besa Security.</p>
        </div>

        <div class="five columns">
            <h3>Sigurimi i tubimeve publike</h3>
            <p>Besa Security ben sigurimin e tubimeve te ndryshme,
            me punetore te sigurimit te uniformuar, te pajisur mire
            dhe te licencuar.</p>
        </div>
    </div>


    <div class="sixteen columns about_posts">
        <div class="about_header">
            <img src="<?php echo THEMEURL; ?>images/theme/about3.jpg" />
        </div>
        <div class="five columns about_item">
            <h3>Mbrojtja e afert e personave VIP</h3>
            <p>Besa Security ben sigurimin e tubimeve te ndryshme, me punetore
            te sigurimit te uniformuar, te pajisur mire dhe te licencuar.</p>
        </div>

        <div class="five columns about_item middle">
            <h3>Transporti i parave te gatshme dhe sendeve me vlere</h3>
            <p>Besa Security me teknologji bashkekohore te monitorimit
            permanent me kamera dhe qasje proaktive ne sigurimin e objekteve
            ben detektimin e hershem te kercenimit/thyerjes dhe menjehere
            dergon patrullat intervenuese per parandalim dhe njofton Policine.</p>
        </div>

        <div class="five columns about_item">
            <h3>Sherbimi shtese i sigurimit me panik buton</h3>
            <p>Sigurimi efikas i objekteve me punetore te sigurimit, brenda
            perimetrit te objektit, te cilet jane te uniformuar, te trajnuar dhe te
            licencuar, me mbeshtetje permanente te Qendres se Kontrollit dhe
            Patrullave Intervenuese te Besa Security.</p>
        </div>
    </div>


    <div class="sixteen columns about_posts">
        <div class="about_header">
            <img src="<?php echo THEMEURL; ?>images/theme/about4.jpg" />
        </div>
        <div class="ten columns about_item">
            <h3>Sigurimi dhe lokalizimi i objekteve dinamike</h3>
            <p>Besa Security me teknologji bashkekohore te monitorimit permanent me kamera dhe qasje proaktive ne sigurimin e objekteve ben detektimin e hershem te kercenimit/thyerjes dhe menjehere dergon patrullat intervenuese per parandalim dhe njofton Policine. Sigurimi efikas i objekteve me punetore te sigurimit, brenda perimetrit te objektit, te
            cilet jane te uniformuar, te trajnuar dhe te licencuar, me mbeshtetje permanente te Qendres se Kontrollit dhe Patrullave Intervenuese te Besa Security.</p>
        </div>

        <div class="five columns about_item">
            <h3>Sigurimi i kontrollit te qasjes</h3>
            <p>Sigurimi efikas i objekteve me punetore te sigurimit,
            brenda perimetrit te objektit, te cilet jane te uniformuar,
            te trajnuar dhe te licencuar, me mbeshtetje permanente
            te Qendres se Kontrollit dhe Patrullave Intervenuese te
            Besa Security.</p>
        </div>
    </div>
</div><!-- /.container -->

<?php get_footer(); ?>