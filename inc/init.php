<?php
/* Register Scripts */
wp_register_script('bir_vars', WPURL . 'index.php?jsvars=true', null, null);

/* Load Vars JS */
if(isset($_GET['jsvars']))
{
	header('Content-Type: text/javascript');
	include THEMEDIR . 'js/vars.js';
	exit;
}

/* Resources import on Frontend */
wp_enqueue_script('bir_vars');
?>