<?php
/*
 * Template Name: Stafi
 *
 * @package WordPress
 * @subpackage Panda
 * @since Panda 1.0
 */

get_header(); ?>
<div id="top_header">
   <?php
   $header_image = get_field('header_image');
   ?>
   <img src="<?php echo $header_image; ?>" width="100%" height="150px" />
</div>

<div class="container" id="staf_container">
    <div class="one-third column staff">
        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/theme/john.png" alt=""></figure>
        <h4>JOHN SMITH</h4>
        <strong>CEO</strong>
    </div>
    <div class="one-third column staff">
        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/theme/david.png" alt=""></figure>
        <h4>DAVID MICHAEL</h4>
        <strong>FOUNDER</strong>
    </div>
    <div class="one-third column staff">
        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/theme/miller.png" alt=""></figure>
        <h4>MILLER SMITH</h4>
        <strong>FOUNDER</strong>
    </div>

    <div class="one-third column staff">
        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/theme/brad.png" alt=""></figure>
        <h4>BRAD PITTER</h4>
        <strong>MANAGER</strong>
    </div>
    <div class="one-third column staff">
        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/theme/mila.png" alt=""></figure>
        <h4>MILA ROGERS</h4>
        <strong>MANAGER</strong>
    </div>
    <div class="one-third column staff">
        <figure><img src="<?php echo get_template_directory_uri(); ?>/images/theme/joe.png" alt=""></figure>
        <h4>JOE CRUSH</h4>
        <strong>MARKETING DIRECTOR</strong>
    </div>

</div><!-- /.container -->

<?php get_footer(); ?>